clear;clc;
load ('response.mat');
load('impulse_response.mat');
h_filter_200tap = g;
%% Load analysis filter taps
load('data/143M.mat');
h143 = h;
load('data/250M.mat');
h250 = h;
load('data/500M.mat');
h500 = h;
%% freq. response of analysis filters
RFFIR_length = length(h143);                        % Analysis filter length
freq_samp = 512;                                     % Freq. samples in -pi to pi
freq_bound = pi;                                    % Interested range [pi]
freq_range = linspace(-pi, pi, freq_samp);          % Freq. of interest
freq_shift_range = 0:3;                             % Sub-channel freq. shift (in BB)
M = length(freq_shift_range);                       % Number of sub-channels
P = M;                                              % Aliasing terms
chan_num = 4;
response = zeros(freq_samp, M);
for ff = 1:freq_samp
    freq = freq_range(ff);
    for mm = 1:P
        freq_shift = freq_shift_range(mm);
        response(ff,mm) = sum(h143.*exp(1j*freq_shift*(2*pi/M)*(0:RFFIR_length-1).')...
            .*exp(1j*freq*(0:RFFIR_length-1).'));
    end
end
%%
error_sigma = 1e-3;
res_design = response(:,1);
for mm=1:chan_num
    res_error(:,mm) = response(:,1) + error_sigma * (randn(freq_samp,1) + 1j * randn(freq_samp,1));
end
figure
subplot(211)
plot(freq_range/pi,20*log10(abs(res_design))-max(20*log10(abs(res_design))),'linewidth',2);hold on
plot(freq_range/pi,20*log10(abs(res_error(:,mm))),'linewidth',2)
xlabel('Freq [\pi]')
ylabel('Response [dB]')
grid on
xlim([0,0.9]);
legend('Designed Analysis Filter','True Analysis Filter')
subplot(212)
plot(freq_range/pi,20*log10(abs(res_error(:,mm)-res_design)),'linewidth',2)
xlabel('Freq [\pi]')
ylabel('Response [dB]')
grid on
xlim([0,0.9]);
title('Diff. b/w True and Knowledge Response')
ylim([-100,0]);
%% 
ite_num = 2e3;
res_est = zeros(freq_samp,ite_num+1, chan_num);
Y_HFB_out = zeros(freq_samp, ite_num);
for mm=1:chan_num
    res_est(:,1,mm) = res_design;
end
mu = 1e-3;

for ite_idx = 1:ite_num
    for mm=1:chan_num
        for q = 1:128
            res_idx = q + (0:(chan_num-1))*128;
            symb = [0; randn((chan_num-1),1) + 1j*randn((chan_num-1),1)];
            symb_WS = symb'*res_error(res_idx, mm);
            symb_WS_exp = symb'*squeeze(res_est(res_idx, ite_idx, mm));
            symb_diff = (symb_WS - symb_WS_exp);
            res_est(res_idx, ite_idx+1, mm) = squeeze(res_est(res_idx, ite_idx, mm))...
                                                + mu * symb * symb_diff;
        end
        learning_curve(ite_idx, mm) = norm(squeeze(res_est(257:512, ite_idx, mm)) -...
                                           res_error(257:512, mm))^2;
    end
    
    for q = 1:64
        res_idx = q + 256 + (0:(chan_num-1))*64;
        for mm = 1:chan_num
            H_mtx(mm,:) = circshift(res_error(res_idx, mm), (mm-1));
            H_mtx_info(mm,:) = circshift(squeeze(res_est(res_idx, ite_idx, mm)), (mm-1));
        end
        F_mtx = pinv(H_mtx_info);
        Y_HFB_out(res_idx, ite_idx) = (F_mtx * H_mtx) * [(randn+1j*randn); zeros((chan_num-1),1)];
        
    end
    
end
%
figure
semilogy(learning_curve)
grid on
xlabel('Iteration Number')
ylabel('Learning Curve')
%%
% nImages = 200;
% % n_range = floor(linspace(0,length(Rx_sig_H1),nImages));
% 
% fig = figure;
% for idx = 1:nImages
%     
%     plot(linspace(250,500,freq_samp),ones(freq_samp,1)*(-80),'k--','linewidth',2);hold on
%     plot(20*log10(abs(Y_HFB_out(:,idx*10))),'linewidth',2);hold off
%     
%     
% %     set(ht,'String',num2str(idx))
%     grid on
%     xlabel('Freq')
%     ylabel('PSD')
%     ylim([-100,10])
%     title(['Iteration ', num2str(floor(idx/10)*10)])
% %     xlim([0,100])
% 
%    
%     drawnow limitrate 
%     frame = getframe(fig);
%     im{idx} = frame2im(frame);
% end
% 
% % figure
% % plot(freq_range, 20*log10(abs(Y_HFB_out(:,100))));

%%
figure
subplot(211)
plot(freq_range/pi,20*log10(abs(res_est(:,end))),'linewidth',2);hold on
plot(freq_range/pi,20*log10(abs(res_error(:,1))),'linewidth',2)

xlabel('Freq [\pi]')
ylabel('Response [dB]')
grid on
xlim([0,0.95]);
legend('Est. Analysis Filter','True Analysis Filter');
subplot(212)
plot(freq_range/pi,20*log10(abs(res_error(:,1)-squeeze(res_est(:,end,1)))),'linewidth',2);
xlabel('Freq [\pi]')
ylabel('Response [dB]')
grid on
xlim([0,0.95]);
title('Diff. b/w True and Knowledge Response');

%%
blue = [   0    0.4470    0.7410];
red =  [    0.8500    0.3250    0.0980];
nImages = 80;
% n_range = floor(linspace(0,length(Rx_sig_H1),nImages));

fig = figure('units','inch','position',[0,0,8,4]);
for idx = 70
    
    % ---- subplot --------
    
    subplot(121)
    plot(freq_range/pi*2e3,20*log10(abs(res_error(:,1))),'color',blue,'linewidth',2);hold on
    plot(freq_range/pi*2e3,20*log10(abs(squeeze(res_est(:,idx * 20, 1)))),'color',red,'linewidth',2);hold off
    set(gca,'FontSize',14)
    xlabel('Freq [MHz]');
    ylabel('Analog Response [dB]');
    title(['Iteration ', num2str(floor(idx/5)*5)],'Units', 'normalized', 'Position', [1.2, 1.0, 0])
    grid on
    xlim([0,2e3*0.95]);
    ylim([-90,0])
    legend({'True Filter Response','Estimated Response'},'Location','south','FontSize',11);

%     thetalim([90,270])
    
    % ---- subplot --------
    subplot(122)
    plot(linspace(0,2e3,freq_samp),ones(freq_samp,1)*(-70),'k--','linewidth',2);hold on
    plot(linspace(-2e3,2e3,freq_samp), 20*log10(abs(Y_HFB_out(:,idx*20))),'color',red,'linewidth',2);hold off
    set(gca,'FontSize',14)
    grid on
    xticks([250,750,1250,1750])
    xticklabels({'C1','C2','C3','C4'})
%     xlabel('Freq')
    ylabel('PSD [dB]')
    ylim([-100,10])
%     title(['Iteration ', num2str(floor(idx/5)*5)])
    legend({'Target','Post Aliasing Compensation'},'Location','south','FontSize',11)
%     xlim([0,100])

    
%     subplot(212)
%     plot(freq_range/pi,20*log10(abs(res_error(:,1)-squeeze(res_est(:,idx * 20,1)))),'linewidth',2);
%     xlabel('Freq [\pi]')
%     ylabel('Response [dB]')
%     grid on
%     xlim([0,0.95]);
%     ylim([-100,-50])
%     legend('Diff. b/w True and Knowledge Response');
% 
%     
    drawnow limitrate 
    frame = getframe(fig);
    im{idx} = frame2im(frame);
end
% close;


%% 
filename = 'testAnimated.gif'; % Specify the output file name
for idx = 1:nImages
    [A,map] = rgb2ind(im{idx},256);
    if idx == 1
        imwrite(A,map,filename,'gif','LoopCount',Inf,'DelayTime',1);
    else
        imwrite(A,map,filename,'gif','WriteMode','append','DelayTime',0.15);
    end
end