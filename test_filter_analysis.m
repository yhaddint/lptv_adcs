clear;clc;

%% Load analysis filter taps
load('data/143M2.mat');
h143 = h;
load('data/250M.mat');
h250 = h;
load('data/500M.mat');
h500 = h;

%% freq. response of analysis filters
fvtool(h143,1);

%% freq. response of analysis filters
RFFIR_length = length(h143);                        % Analysis filter length
freq_samp = 64;                                     % Freq. samples in -pi to pi
freq_bound = pi;                                    % Interested range [pi]
freq_range = linspace(-pi, pi, freq_samp);          % Freq. of interest
freq_shift_range = 0:3;                             % Sub-channel freq. shift (in BB)
M = length(freq_shift_range);                       % Number of sub-channels
P = M;                                              % Aliasing terms

response = zeros(freq_samp, M);
for ff = 1:freq_samp
    freq = freq_range(ff);
    for mm = 1:P
        freq_shift = freq_shift_range(mm);
        response(ff,mm) = sum(h143.*exp(1j*freq_shift*(2*pi/M)*(0:RFFIR_length-1).')...
            .*exp(1j*freq*(0:RFFIR_length-1).'));
    end
end
figure
semilogx(freq_range/pi,20*log10(abs(response)),'linewidth',2)
xlabel('Freq [\pi]')
ylabel('Response [dB]')
grid on
xlim([-0.99,0.99]);
ylim([-100,0]);

%% Parameters for optimziation 
d_target = 40;                                  % Target group delay in BB
N_taps = 80;                                    % Target synthesize filter taps
% x_vec = [];                                     % Opt variable to determine
c_vec = zeros(N_taps*M + 1,1);   
c_vec(1) = 1;

% Target response of cascaded system
g_mtx = zeros(P, length(freq_range));
g_mtx(1,:) = exp(-1j * freq_range * d_target);

% Vector r and i in eq(7)
F_Re_mtx = zeros( P, freq_samp, M*N_taps );
F_Im_mtx = zeros( P, freq_samp, M*N_taps );


Freq_mtx = (cos(freq_range.'*(0:N_taps-1)) - 1j*sin(freq_range.'*(0:N_taps-1)));
eps = 1e-2; % value to make mod functioin hits boundary

for ff = 1:freq_samp
    for pp = 1:P
        for mm=1:M
            
            % Indecies in vector r_p and i_p before eq(6)
            vec_idx = (mm-1)*N_taps+1 : mm*N_taps;
            
            % Find the shifted response of M channlized RF filter
            freq_shift_p = round(mod((ff-(pp-1)*freq_samp/M)-eps, freq_samp) + eps);
            
            % follow description before eq(6)
            F_Re_mtx(pp, ff, vec_idx) = real( (response(freq_shift_p,mm).') * Freq_mtx(ff,:).' );
            F_Im_mtx(pp, ff, vec_idx) = imag( (response(freq_shift_p,mm).') * Freq_mtx(ff,:).' );
        end
    end
end
%% Optimization via SOCP
cvx_begin
    variable x_vec(641,1)
    minimize(c_vec.'*x_vec);
    for pp = 1:P
        for ff=1:freq_samp
            weight = (freq_range(ff)>-0.8*pi) & (freq_range(ff)<0.8*pi);
            c_vec.'*x_vec >= norm([0,squeeze(F_Re_mtx(pp,ff,:)).';0,squeeze(F_Im_mtx(pp,ff,:)).']...
                        *x_vec - [real(g_mtx(pp,ff));imag(g_mtx(pp,ff))],2) * weight;
        end
    end
cvx_end


%%
response_FIR = zeros(P,freq_samp);
for pp=1:P
    for ff=1:freq_samp
        response_FIR(pp,ff) = norm([squeeze(F_Re_mtx(pp,ff,:)).';squeeze(F_Im_mtx(pp,ff,:)).']...
                        *x_vec(2:end),2)^2;
    end
end
figure
subplot(311)
plot(freq_range/pi, 10*log10(response_FIR(1,:)),'linewidth',2);hold on
xlabel('Freq [pi]')
ylabel('Gain [dB]')
grid on

subplot(312)
plot(freq_range/pi, phase(response_FIR(1,:)),'linewidth',2);hold on
xlabel('Freq [pi]')
ylabel('Phase [rad]')

subplot(313)
plot(freq_range/pi, 10*log10(response_FIR(2,:)),'linewidth',2);hold on
plot(freq_range/pi, 10*log10(response_FIR(3,:)),'linewidth',2);hold on
plot(freq_range/pi, 10*log10(response_FIR(4,:)),'linewidth',2);hold on
plot(freq_range/pi, 10*log10(response_FIR(5,:)),'linewidth',2);hold on
plot(freq_range/pi, 10*log10(response_FIR(6,:)),'linewidth',2);hold on
plot(freq_range/pi, 10*log10(response_FIR(7,:)),'linewidth',2);hold on
plot(freq_range/pi, 10*log10(response_FIR(8,:)),'linewidth',2);hold on

grid on
legend('T_1','T_2','T_3','T_4','T_5','T_6','T_7')
xlabel('Freq [pi]')
ylabel('Gain [dB]')
