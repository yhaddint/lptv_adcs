clear all;clc;
cvx_solver sedumi

%% Load analysis filter taps
load('data/143M2.mat');
h143m2 = h;
load('data/143M.mat');
h143 = h;
load('data/250M.mat');
h250 = h;
load('data/500M.mat');
h500 = h; 
load('data/h1.mat');
h1 = h;
load('data/hButterworth.mat');
hIIR = impz(b,a,100);
load('data/hIIR_trunc.mat');
% hIIR = impz(b,a,200);

%% freq. response of analysis filters
% b1 = fir1(31, 1/6, 'low');
% f = [0 1/6 1/4 1];
% mhi = [1 1 0 0];
% b1 = fir2(16,f,mhi);
% fvtool(h143m2,1);
h1 = h143m2;
% fvtool(h2,1);

%% freq. response of analysis filters
RFFIR_length = length(h1);                          % Analysis filter length
freq_samp = 150;                                    % Freq. samples in -pi to pi
freq_bound = pi;                                    % Interested range [pi]
freq_range_wrap = linspace(-pi, pi, freq_samp+1);   % Freq. of interest
freq_range = freq_range_wrap(2:end);
M = 8;                                              % Number of sub-channels
freq_shift_range = 0:(M-1);                         % Sub-channel freq. shift (in BB)
P = M;                                              % Aliasing terms

response = zeros(freq_samp, M);

% random mismatch generation
NN = 6; % number of bits
hunit = max(h1)/(2^NN - 1);
code = (h1./hunit);
code = round(code);
hq = hunit.*code;
r = 0 + hunit*randn(P*RFFIR_length,1)/3; %upper bound 3 sigma to be 10-bit

% all filters have different radom mismatch
for mm = 1:M
   hqr(:,mm) = h1 + r((mm - 1)*RFFIR_length + 1:mm*RFFIR_length);
end

% all filters have the same mismatch
% h1 = h1 + r(1:16);
%% Filter shape comparison
for pp = 1:P
    for ff = 1:freq_samp
        freq = freq_range(ff);
        for mm = 1:M
            freq_shift = freq_shift_range(mm);
            response(ff,mm,pp) = sum(hqr(:,mm)...
                .*exp(-1j*(freq - 2*pi*(pp - 1)/M)*(0:RFFIR_length-1).'));

        end
    end
end
figure
plot(freq_range/pi,20*log10(abs(response(:,1:3,1))),'linewidth',2)
set(gca,'FontSize',14)
xlabel('Freq [\pi]')
ylabel('Response [dB]')
grid on
xlim([0,1]);
ylim([-80,-50]);

legendtext = {};
for mm = 1:M
    legendtext{mm} = ['Ch ' num2str(mm,'%d')];
end

legend(legendtext,'Location','eastoutside','FontSize',11)
%%
% calculate response using mismatched analysis filters
for pp = 1:P
    for ff = 1:freq_samp
        freq = freq_range(ff);
        for mm = 1:M
            freq_shift = freq_shift_range(mm);
% here the "if mm == 1" and "if mm == 1 || mm == P" are mutually explusive
% to each other, used for the highest-frequency channel centered at <pi or
% at exactly pi
%             if mm == 1
%                 response(ff,mm,pp) = sum(hqr(:,pp).*cos(2*freq_shift*(pi/(2*M-1))*(0:RFFIR_length-1).').*exp(-1j*(freq - 2*pi*(pp - 1)/M)*(0:RFFIR_length-1).'));
%             else
%                 response(ff,mm,pp) = sum(2*hqr(:,pp).*cos(2*freq_shift*(pi/(2*M-1))*(0:RFFIR_length-1).').*exp(-1j*(freq - 2*pi*(pp - 1)/M)*(0:RFFIR_length-1).'));
%             end
            if mm == 1 || mm == M
                response(ff,mm,pp) = sum(hqr(:,mm).*cos(freq_shift*(pi/(M-1))*(0:RFFIR_length-1).')...
                    .*exp(-1j*(freq - 2*pi*(pp - 1)/M)*(0:RFFIR_length-1).'));
            else
                response(ff,mm,pp) = sum(2*hqr(:,mm).*cos(freq_shift*(pi/(M-1))*(0:RFFIR_length-1).')...
                    .*exp(-1j*(freq - 2*pi*(pp - 1)/M)*(0:RFFIR_length-1).'));
            end
        end
    end
end

% calculate response using identical analysis filters
for pp = 1:P
    for ff = 1:freq_samp
        freq = freq_range(ff);
        for mm = 1:M
            freq_shift = freq_shift_range(mm);
%             if mm == 1
%                 response(ff,mm,pp) = sum(h1.*cos(2*freq_shift*(pi/(2*M-1))*(0:RFFIR_length-1).').*exp(-1j*(freq - 2*pi*(pp - 1)/M)*(0:RFFIR_length-1).'));
%             else
%                 response(ff,mm,pp) = sum(2*h1.*cos(2*freq_shift*(pi/(2*M-1))*(0:RFFIR_length-1).').*exp(-1j*(freq - 2*pi*(pp - 1)/M)*(0:RFFIR_length-1).'));
%             end
            if mm == 1 || mm == M
                response_ideal(ff,mm,pp) = sum(h1.*cos(freq_shift*(pi/(M-1))*(0:RFFIR_length-1).').*exp(-1j*(freq - 2*pi*(pp - 1)/M)*(0:RFFIR_length-1).'));
            else
                response_ideal(ff,mm,pp) = sum(2*h1.*cos(freq_shift*(pi/(M-1))*(0:RFFIR_length-1).').*exp(-1j*(freq - 2*pi*(pp - 1)/M)*(0:RFFIR_length-1).'));
            end
        end
    end
end

% 2nd-order Butterworth (don't need to care)
% for pp = 1:P
%     for ff = 1:freq_samp
%         freq = freq_range(ff);
%         for mm = 1:P
%             freq_shift = freq_shift_range(mm);
%             if mm == 1 || mm == P
%                 response(ff,mm,pp) = sum(hIIR.*cos(freq_shift*(pi/(M-1))*(0:length(hIIR)-1).').*exp(-1j*(freq - 2*pi*(pp - 1)/M)*(0:length(hIIR)-1).'));
%             else
%                 response(ff,mm,pp) = sum(2*hIIR.*cos(freq_shift*(pi/(M-1))*(0:length(hIIR)-1).').*exp(-1j*(freq - 2*pi*(pp - 1)/M)*(0:length(hIIR)-1).'));
%             end
%         end
%     end
% end

figure
plot(freq_range/pi,20*log10(abs(response(:,:,1))),'linewidth',2)
% plot(freq_range/pi,20*log10(abs(response(:,3,3))),'linewidth',2)
% hold on
% plot(freq_range/pi,20*log10(abs(response(:,3,7))),'linewidth',2)
set(gca,'FontSize',14)
xlabel('Freq [\pi]')
ylabel('Response [dB]')
grid on
xlim([-1,1]);
ylim([-100,5]);

legendtext = {};
for mm = 1:M
    legendtext{mm} = ['Ch ' num2str(mm,'%d')];
end

legend(legendtext,'Location','eastoutside','FontSize',11)

delay_range = 50;
for tap_idx = 1:length(delay_range)
% tps = 40;
d_target = delay_range(tap_idx);

%% Parameters for optimziation 
% d_target = 55;                                  % Target group delay in BB
N_taps = d_target*2;                        % Target synthesize filter taps
% x_vec = [];                                   % Opt variable to determine
c_vec = zeros(N_taps*M + 1,1);
c_vec(1) = 1;

% Target response of cascaded system
g_mtx = zeros(P, length(freq_range));
g_mtx(1,:) = exp(-1j * freq_range * d_target);

% Vector r and i in eq(7)
F_Re_mtx = zeros(P, freq_samp, M*N_taps);
F_Im_mtx = zeros(P, freq_samp, M*N_taps);
F_Re_mtx_ideal = zeros(P, freq_samp, M*N_taps);
F_Im_mtx_ideal = zeros(P, freq_samp, M*N_taps);


Freq_mtx = (cos(freq_range.'*(0:N_taps-1)) - 1j*sin(freq_range.'*(0:N_taps-1)));
% eps = 1e-2; % value to make mod functioin hits boundary

for ff = 1:freq_samp
    for pp = 1:P
        for mm=1:M
            
            % Indecies in vector r_p and i_p before eq(6)
            vec_idx = (mm-1)*N_taps+1 : mm*N_taps;
            
            % Find the shifted response of M channlized RF filter
%             freq_shift_p = round(mod((ff-(pp-1)*freq_samp/M)-eps, freq_samp) + eps);
            
            % follow description before eq(6)
            F_Re_mtx(pp,ff,vec_idx) = real((response(ff,mm,pp).')*Freq_mtx(ff,:).')/M;
            F_Im_mtx(pp,ff,vec_idx) = imag((response(ff,mm,pp).')*Freq_mtx(ff,:).')/M;
            
            F_Re_mtx_ideal(pp,ff,vec_idx) = real((response_ideal(ff,mm,pp).')*Freq_mtx(ff,:).')/M;
            F_Im_mtx_ideal(pp,ff,vec_idx) = imag((response_ideal(ff,mm,pp).')*Freq_mtx(ff,:).')/M;
        end
    end
end
%% Optimization via SOCP
cvx_begin
% cvx_precision best
    variable x_vec(N_taps*M + 1,1)
    minimize(c_vec.'*x_vec);
    for pp = 1:P
        for ff=1:freq_samp
            weight = (freq_range(ff)>-0.9*pi) & (freq_range(ff)<0.9*pi);
            if pp == 1
                c_vec.'*x_vec >= 1*norm([0,squeeze(F_Re_mtx_ideal(pp,ff,:)).';...
                    0,squeeze(F_Im_mtx_ideal(pp,ff,:)).']*x_vec - [real(g_mtx(pp,ff));imag(g_mtx(pp,ff))],2)*weight;
            else
                c_vec.'*x_vec >= norm([0,squeeze(F_Re_mtx_ideal(pp,ff,:)).';...
                    0,squeeze(F_Im_mtx_ideal(pp,ff,:)).']*x_vec - [real(g_mtx(pp,ff));imag(g_mtx(pp,ff))],2)*weight;
            end
        end
    end
cvx_end

%% quantization, I guess you can ignore this part for now
% % for i = 14:24
% % B = i;
% 
% B = 12;
% % for ind = 1:P
% %     maxf = max(abs(x_vec((ind - 1)*N_taps + 2:ind*N_taps + 1)));
% %     bfint = ceil(log2(maxf));
% %     if bfint < 0
% %         bfint = 0;
% %     end
% %     y((ind - 1)*N_taps + 1:ind*N_taps) = quantizenumeric(x_vec((ind - 1)*N_taps + 2:ind*N_taps + 1),1,B,B - 1 - bfint,'fix');
% % end
% 
% % quantization scale applies to all filters
% maxf = max(abs(x_vec(2:end)));
% bfint = ceil(log2(maxf));
% if bfint < 0
%     bfint = 0;
% end
% y = quantizenumeric(x_vec(2:end),1,B,B - 1 - bfint,'fix').';

%% Quantize synthesis filters
response_FIR = zeros(P,freq_samp);
for pp=1:P
    for ff=1:freq_samp
        %with quantization
%         response_FIR(pp,ff) = norm([squeeze(F_Re_mtx(pp,ff,:)).';squeeze(F_Im_mtx(pp,ff,:)).']*y.',2)^2;
        
        % without quantization
        response_FIR(pp,ff) = norm([squeeze(F_Re_mtx(pp,ff,:)).';...
                                    squeeze(F_Im_mtx(pp,ff,:)).']*x_vec(2:end),2)^2;
        % without quantization and using ideal filter
%         response_FIR(pp,ff) = norm([squeeze(F_Re_mtx_ideal(pp,ff,:)).';...
%                                     squeeze(F_Im_mtx_ideal(pp,ff,:)).']*x_vec(2:end),2)^2;
    end
end

%I don't quite remember the following commented code, you can ignore them
% Aalias(i - 13) = 10*log10(max(max(abs(response_FIR(2:end,7:194)))));
% end

% plot(14:24,Aalias)
% hold on
% end

figure
subplot(211)
plot(freq_range/pi, 10*log10(response_FIR(1,:)),'linewidth',2);hold on
xlabel('Freq [pi]')
ylabel('Gain [dB]')
grid on
% 
% % subplot(312)
% % plot(freq_range/pi, phase(response_FIR(1,:)),'linewidth',2);hold on
% % xlabel('Freq [pi]')
% % ylabel('Phase [rad]')
% 
subplot(212)
plot(freq_range/pi, 10*log10(response_FIR(2,:)),'linewidth',2);hold on
plot(freq_range/pi, 10*log10(response_FIR(3,:)),'linewidth',2);hold on
plot(freq_range/pi, 10*log10(response_FIR(4,:)),'linewidth',2);hold on
% plot(freq_range/pi, 10*log10(response_FIR(5,:)),'linewidth',2);hold on
% plot(freq_range/pi, 10*log10(response_FIR(6,:)),'linewidth',2);hold on
% plot(freq_range/pi, 10*log10(response_FIR(7,:)),'linewidth',2);hold on
% plot(freq_range/pi, 10*log10(response_FIR(8,:)),'linewidth',2);hold on
% 
grid on
legend('T_1','T_2','T_3')
% 
% legend('T_1','T_2','T_3','T_4','T_5','T_6','T_7')
% xlabel('Freq [pi]')
% ylabel('Gain [dB]')
%% Synthesis Filter response
% mm=3;
% my_mtx = [squeeze(F_Re_mtx(1,:,(mm-1)*N_taps+1 : mm*N_taps));...
%     squeeze(F_Im_mtx(1,:,(mm-1)*N_taps+1 : mm*N_taps))];
% res = my_mtx*x_vec((mm-1)*91+2:mm*91+1);
% res_comp = res(1:200).^2+res(201:400).^2;
% figure;
% plot(freq_range/pi,10*log10(res_comp))
% grid on
% xlim([0,1])
% 
% fvtool(x_vec((mm-1)*91+2:mm*91+1),1)

%%

% freq_samp = 504;                                    % Freq. samples in -pi to pi
% freq_bound = pi;                                    % Interested range [pi]
% freq_range_wrap = linspace(-pi, pi, freq_samp+1);   % Freq. of interest
% freq_range = freq_range_wrap(2:end);
% 
% sig_length = 512;
% sig_length_filt = sig_length - 8;
% start_p = 0;
% x_sig = cos(pi/4*(0:sig_length-1)');% + cos(pi/4*(0:sig_length-1)');%randn(sig_length,1);
% 
% mm=1;
% hqr_upconv = hqr(:,mm).*cos(freq_shift_range(mm)*(pi/(M-1))*(0:RFFIR_length-1).');
% x_out = filter(hqr_upconv,1,x_sig);
% x_out_delay = x_out(9+start_p:end);
% x_out_freq = exp(-1j*freq_range.'*(0:length(x_out_delay)-1))*x_out_delay;
% 
% sig_in_freq = exp(-1j*freq_range.'*(0:sig_length_filt-1-start_p))*x_sig(start_p+1:sig_length_filt);
% % hqr_upconv = hqr(:,2).*cos(freq_shift_range(mm)*(pi/(M-1))*(0:RFFIR_length-1).');
% H2_freq = exp(-1j*freq_range.'*(0:RFFIR_length-1))*hqr_upconv;
% sig_out_freq = sig_in_freq.*H2_freq;
% 
% 
% % subplot(211)
% figure
% plot(freq_range/pi, 20*log10(abs(x_out_freq)));hold on
% plot(freq_range/pi, 20*log10(abs(sig_out_freq)));hold on
% 
% % plot(freq_range/pi, 20*log10(abs(sig_out_freq)))
% 
% xlabel('freq [pi]')
% % xlim([0,1])
% ylim([-40,55])
% grid on
% 
% legend('out','original')
%% Time domain simulation of Filter bank
% sig_length = 2^12;
% sig_length_filt = sig_length;
% x_sig = cos(pi*0.2167/2*(0:sig_length-1)');% + cos(pi/4*(0:sig_length-1)');%randn(sig_length,1);
% clearvars sig_distorted sig_rec
% % sig_distorted = zeros(sig_length_filt,M);
% % sig_rec = zeros(sig_length_filt,M);
% 
% for mm=1:M
%    freq_shift = freq_shift_range(mm);
%    hqr_upconv = hqr(:,mm).*cos(freq_shift*(pi/(M-1))*(0:RFFIR_length-1).');
% %    hqr_upconv = h1.*cos(freq_shift*(pi/(M-1))*(0:RFFIR_length-1).');
%    sig_analog_filter = filter(hqr_upconv, 1, x_sig);
%    sig_downsample = sig_analog_filter(1:M:end);
%    if mm==1 || mm==M
%        sig_distorted(:,mm) = upsample(sig_downsample,M);
%    else
%        sig_distorted(:,mm) = 2*upsample(sig_downsample,M);
%    end
%    
%    synth_filter = x_vec((mm-1)*N_taps+2:mm*N_taps+1);
%    sig_rec(:,mm) = filter(synth_filter,1,sig_distorted(:,mm));
% end
% 
% sig_out = sum(sig_rec(1:end,:),2);
% lbp_final = fir1(200, 0.9, 'low');
% sig_out_lpf = filter(lbp_final,1,sig_out);
% sig_out = sig_out_lpf(400:end);
% 
% sig_distorted_freq = exp(-1j*freq_range.'*(0:length(sig_out)-1))*sig_out/sqrt(length(sig_out));
% %
% % figure
% % % subplot(211)
% % plot(freq_range/pi, 20*log10(abs(sig_distorted_freq)),'linewidth',2)
% % hold on
% % grid on
% % xlabel('freq [pi]')
% % % ylim([-40,35])
% % % xlim([0,1])
% % %
% % sig_in_freq = exp(-1j*freq_range.'*(0:sig_length_filt-1))*x_sig(1:sig_length_filt)/sqrt(sig_length_filt);
% % hqr_upconv = hqr(:,2).*cos(freq_shift_range(2)*(pi/(M-1))*(0:RFFIR_length-1).');
% % H2_freq = exp(-1j*freq_range.'*(0:RFFIR_length-1))*hqr_upconv;
% % sig_out_freq = sig_in_freq.*H2_freq;
% % 
% % % sig_out_freq = exp(-1j*freq_range.'*(0:sig_length_filt-1))*sig_analog_filter;
% % sig_alias = sig_out_freq...
% %     + circshift(sig_out_freq,freq_samp/4)...
% %     + circshift(sig_out_freq,freq_samp/2)...
% %     + circshift(sig_out_freq,freq_samp*3/4);
% % 
% % % subplot(211)
% % plot(freq_range/pi, 20*log10(abs(sig_in_freq)),'linewidth',2)
% % xlabel('freq [pi]')
% % % xlim([0,1])
% % ylim([-40,35])
% % xlim([0,0.9])
% % grid on
% 
% legend('out','original')
% %
% [pxx1,f1] = pwelch(x_sig, blackman(1024), [], 1024, 2);
% [pxx2,f2] = pwelch(sig_out, blackman(1024), [], 1024, 2);
% 
% figure
% % subplot(211)
% plot(f1,10*log10(pxx1),'linewidth',2)
% hold on
% plot(f2,10*log10(pxx2),'--k','linewidth',2)
% hold on
% % plot(f1,10*log10(abs(pxx1-pxx2)),'linewidth',2)
% set(gca,'FontSize',14)
% grid on
% xlabel('Frequency [GHz]')
% ylabel('Estimated PSD [dB]')
% legend('ADC input','ADC output')
% xlim([0,1])
% ylim([-150,30])


%%
sig_length = 2^10+100;
freq_sweep_num = 10*freq_samp+1;
freq_sweep_range = linspace(0,0.8,freq_sweep_num);
worst_surious = zeros(freq_sweep_num,1);
lbp_final = fir1(200, 0.85, 'low');
for freq_idx = 1:freq_sweep_num
    x_sig = cos(pi*freq_sweep_range(freq_idx)*(0:sig_length-1)');% + cos(pi/4*(0:sig_length-1)');%randn(sig_length,1);
    clearvars sig_distorted sig_rec

    for mm=1:M
       freq_shift = freq_shift_range(mm);
       hqr_upconv = hqr(:,mm).*cos(freq_shift*(pi/(M-1))*(0:RFFIR_length-1).');
       sig_analog_filter = filter(hqr_upconv, 1, x_sig);
       sig_downsample = sig_analog_filter(1:M:end);
       if mm==1 || mm==M
           sig_distorted(:,mm) = upsample(sig_downsample,M);
       else
           sig_distorted(:,mm) = 2*upsample(sig_downsample,M);
       end

       synth_filter = x_vec((mm-1)*N_taps+2:mm*N_taps+1);
       sig_rec(:,mm) = filter(synth_filter,1,sig_distorted(:,mm));
    end

    sig_out = sum(sig_rec(1:end,:),2);
    sig_out_lpf = filter(lbp_final,1,sig_out);
%     sig_out_lpf = filter(lbp_final,1,sig_out_lpf1);

    sig_out = sig_out_lpf(151:end);
    
%     figure
%     plot(sig_out)
%     hold on
%     plot(x_sig)
%     grid on
    csvwrite('synth_ideal_8chan.csv', x_vec)
    csvwrite('LPF_8chan.csv', lbp_final)

    [pxx2,f2] = pwelch(sig_out, blackman(1024), [], 1024, 2);
    f2_new = f2(find(f2<0.9));
    useful_wind = find(f2<0.9);
    [peak_value,maxidx] = max(10*log10(pxx2(useful_wind)));
    search_wind = [find(f2_new<f2(maxidx)-0.05);find(f2_new>f2(maxidx)+0.05)];
    worst_surious(freq_idx) = peak_value - max(10*log10(pxx2(search_wind)));
    
end

range_used = min(find(freq_sweep_range>0)):max(find(freq_sweep_range<0.9));
SFDR(tap_idx) = min(worst_surious(range_used));
end
%%
figure
plot(delay_range*2,-SFDR,'-o','linewidth',2);
grid on
xlabel('Num. of Synthesis Filter Taps')
ylabel('Spurious Suppression [dB]')
% figure
% plot(freq_sweep_range*2, worst_surious,'linewidth',2)
% set(gca,'FontSize',14)
% hold on
% grid on
% xlim([0,1.8])
% xticks(linspace(0,1.8,7))
% xlabel('Input Tone Frequency [GHz]')
% ylabel('Spurious Suppression [dB]')

%%
% Fs = 2e9;
% NFFT = length(sig_out);
% 
% % Power spectrum is computed when you pass a 'power' flag input
% [P,F] = periodogram(x_sig,[],NFFT,Fs,'power');
% 
% helperFrequencyAnalysisPlot2(F,10*log10(P),'Frequency in Hz',...
%   'Power spectrum (dBW)',[],[],[-0.5 1e9])
%%
% sig_length = 512;
% x_sig = cos(pi/8*(0:sig_length-1)') + cos(pi/4*(0:sig_length-1)');%randn(sig_length,1);
% sig_in_freq = exp(-1j*freq_range.'*(0:sig_length-1))*x_sig;
% figure
% plot(freq_range/pi, 20*log10(abs(sig_in_freq)))
% xlabel('freq [pi]')
% % xlim([0,1])
% ylim([-40,60])
% grid on
%% compare input and output of filter banks
% sig_out = sum(sig_rec(46:end,:),2);
% figure;
% plot(x_sig(100:200),'linewidth',2)
% hold on
% plot(sig_out(100:200)*(norm(x_sig)/norm(sig_out)),'-','linewidth',2)
% grid on
% legend('Input','Filter Bank')