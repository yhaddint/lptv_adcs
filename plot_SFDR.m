load('data/SFDR_run1.mat');
SFDR_run1 = SFDR_vs_taps;
load('data/SFDR_run2.mat');
SFDR_run2 = SFDR_vs_taps;
load('data/SFDR_run3.mat');
SFDR_run3 = SFDR_vs_taps;
load('data/SFDR_run1_freq_sample_200.mat');
SFDR_run4 = SFDR_vs_taps;
load('data/SFDR_run1_freq_sample_100.mat');
SFDR_run5 = SFDR_vs_taps;
load('data/SFDR_run1_freq_sample_100_mismatch.mat');
SFDR_run6 = SFDR_vs_taps;
load('data/SFDR_run1_freq_sample_150_mismatch.mat');
SFDR_run7 = SFDR_vs_taps;
load('data/SFDR_run1_freq_sample_100_mismatch_12bits.mat');
SFDR_run8 = SFDR_vs_taps;
figure
% plot(SFDR_run2(1,:),-SFDR_run2(2,:),'-o','linewidth',2);
% hold on
% plot(SFDR_run3(1,:),-SFDR_run3(2,:),'-o','linewidth',2);
% hold on
plot(SFDR_run5(1,:),SFDR_run5(2,:),'k-o','linewidth',2);
hold on
plot(SFDR_run1(1,:),SFDR_run1(2,:),'r-o','linewidth',2);
hold on
plot(SFDR_run4(1,:),SFDR_run4(2,:),'-o','linewidth',2);
hold on
grid on
set(gca,'FontSize',14)
xlabel('Num. of Synthesis Filter Taps')
ylabel('Spurious Suppression [dB]')
xlim([80,150])
legend('100 Freq Sample', '150 Freq Sample', '200 Freq Sample')

figure
plot(SFDR_run5(1,:),SFDR_run5(2,:),'k-o','linewidth',2);
hold on
% plot(SFDR_run1(1,:),SFDR_run1(2,:),'r-o','linewidth',2);
% hold on
plot(SFDR_run6(1,:),SFDR_run6(2,:),'k--x','linewidth',2);
hold on
% plot(SFDR_run7(1,:),SFDR_run7(2,:),'r--x','linewidth',2);
% hold on
plot(SFDR_run8(1,:),SFDR_run8(2,:),'k-.s','linewidth',2);
hold on

grid on
set(gca,'FontSize',14)
xlabel('Num. of Synthesis Filter Taps')
ylabel('Spurious Suppression [dB]')
xlim([80,150])
% legend('100 Freq Sample (10bits; ideal knowledge)',...
%     '150 Freq Sample (10bits; ideal knowledge)',...
%     '100 Freq Sample (10bits; nonideal knowledge)',...
%     '150 Freq Sample (10bits; nonideal knowledge)',...
%     '100 Freq Sample (12bits; nonideal knowledge)')
legend('100 Freq Sample (10bits; ideal knowledge)',...
    '100 Freq Sample (10bits; nonideal knowledge)',...
    '100 Freq Sample (12bits; nonideal knowledge)')