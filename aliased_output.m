clear all;clc;
cvx_solver sedumi

%% Load analysis filter taps
load('data/143M2.mat');
h143m2 = h;
% load('data/143M.mat');
% h143 = h;
% load('data/250M.mat');
% h250 = h;
% load('data/500M.mat');
% h500 = h; 
% load('data/h1.mat');
% h1 = h;
% load('data/hButterworth.mat');
% hIIR = impz(b,a,100);
% load('data/hIIR_trunc.mat');
% hIIR = impz(b,a,200);
% fvtool(h143m2,1)
h1 = h143m2;

load('data/g143.mat');
h_mismatch = g;
fvtool(h1)

%% freq. response of analysis filters
RFFIR_length = length(h1);                          % Analysis filter length
freq_samp = 160;                                    % Freq. samples in -pi to pi
freq_bound = pi;                                    % Interested range [pi]
freq_range_wrap = linspace(-pi, pi, freq_samp+1);   % Freq. of interest
freq_range = freq_range_wrap(2:end);
M = 8;                                              % Number of sub-channels
freq_shift_range = 0:(M-1);                         % Sub-channel freq. shift (in BB)
P = M;                                              % Aliasing terms

response = zeros(freq_samp, M);

% random mismatch generation
NN = 8; % number of bits
hunit = max(h1)/(2^NN - 1);
code = (h1./hunit);
code = round(code);
hq = hunit.*code;
r = 0 + hunit*randn(P*RFFIR_length,1)/3; %upper bound 3 sigma to be 10-bit

% all filters have different radom mismatch
for mm = 1:M
    % mismatch due to quantization
    hqr(:,mm) = h1 + r((mm - 1)*RFFIR_length + 1:mm*RFFIR_length);
    
    % mismatch due to systematic error
%     hqr(:,mm) = h_mismatch;
end
%%
sig_length = 2^11 + 300;
cut_first = 20;
freq_sweep_num = 10*freq_samp+1;
freq_sweep_range = linspace(0,0.8,freq_sweep_num);
worst_surious = zeros(freq_sweep_num,1);
lbp_final = fir1(200, 0.85, 'low');
sig_target = zeros(2^11*freq_sweep_num,1);
for freq_idx = 1:freq_sweep_num
    
    % Tone signal as supervised signal
    x_sig = cos(pi*freq_sweep_range(freq_idx)*(0:sig_length-1)');% + cos(pi/4*(0:sig_length-1)');%randn(sig_length,1);
      
    samp_idx = (freq_idx-1)*2^11+1: freq_idx*2^11;

    for mm=1:M
       freq_shift = freq_shift_range(mm);
       hqr_upconv = hqr(:,mm).*cos(freq_shift*(pi/(M-1))*(0:RFFIR_length-1).');
       sig_analog_filter = filter(hqr_upconv, 1, x_sig);
       sig_downsample = sig_analog_filter(1:M:end);
       if mm==1 || mm==M
           temp = upsample(sig_downsample,M);
           sig_distorted(samp_idx,mm) = temp(cut_first + (1:2^11));
       else
           temp = 2*upsample(sig_downsample,M);
           sig_distorted(samp_idx,mm) = temp(cut_first + (1:2^11));
       end

%        synth_filter = x_vec((mm-1)*N_taps+2:mm*N_taps+1);
%        sig_rec(:,mm) = filter(synth_filter,1,sig_distorted(:,mm));
    end
    
    temp_target = filter(lbp_final,1,x_sig);
    temp2_target = filter(lbp_final,1,temp_target);
    sig_target(samp_idx) = temp2_target(cut_first + 200 + (1:2^11));

end

%%
train_sig_target = zeros(2^11*freq_sweep_num,1);
for freq_idx = 1:freq_sweep_num
    
    % Random signal as supervised signal
    x_sig = randn(sig_length,1);

    samp_idx = (freq_idx-1)*2^11+1: freq_idx*2^11;

    for mm=1:M
       freq_shift = freq_shift_range(mm);
       hqr_upconv = hqr(:,mm).*cos(freq_shift*(pi/(M-1))*(0:RFFIR_length-1).');
       sig_analog_filter = filter(hqr_upconv, 1, x_sig);
       sig_downsample = sig_analog_filter(1:M:end);
       if mm==1 || mm==M
           temp = upsample(sig_downsample,M);
           testsig_distorted(samp_idx,mm) = temp(cut_first + (1:2^11));
       else
           temp = 2*upsample(sig_downsample,M);
           testsig_distorted(samp_idx,mm) = temp(cut_first + (1:2^11));
       end

    end
    
    temp_target = filter(lbp_final,1,x_sig);
    temp2_target = filter(lbp_final,1,temp_target);
    testsig_target(samp_idx) = temp2_target(cut_first + 200 + (1:2^11));

end

%%
% figure
% plot(sig_distorted(1024*2+1:1024*2+1+1024-1,1))
% sig_distorted(1024*2+1:1024*2+1+50-1,1)
% hold on
% plot(sig_distorted(1:1024,2))
% hold on
% plot(sig_distorted(1:1024,3))
% hold on
% plot(sig_distorted(1:1024,4))


%%
save_CSV = 1;
save_dir = '../data/' % Directory for csv files

if save_CSV

    % save channel output
    file_name = [save_dir(2:end),'channel_output_8chan.csv'];
    fprintf('Save file %s \n', file_name)
    csvwrite(file_name, sig_distorted)

    % save target
    file_name = [save_dir(2:end),'target_wave_8chan.csv'];
    fprintf('Save file %s \n', file_name)
    csvwrite(file_name, sig_target)
    fprintf('Done\n')
    
    % save channel output
    file_name = [save_dir(2:end),'channel_output_8chan_random.csv'];
    fprintf('Save file %s \n', file_name)
    csvwrite(file_name, testsig_distorted)

    % save target
    file_name = [save_dir(2:end),'target_wave_8chan_random.csv'];
    fprintf('Save file %s \n', file_name)
    csvwrite(file_name, testsig_target)
    fprintf('Done\n')
end
    
