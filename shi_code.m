clear all;clc;
cvx_solver sedumi

%% Load analysis filter taps
load('data/143M2.mat');
h143 = h;
load('data/250M.mat');
h250 = h;
load('data/500M.mat');
h500 = h;
load('data/h1.mat');
h1 = h;
load('data/hButterworth.mat');
hIIR = impz(b,a,100);
load('data/hIIR_trunc.mat');
% hIIR = impz(b,a,200);

%% freq. response of analysis filters
% fvtool(h143,1);

%% freq. response of analysis filters
RFFIR_length = length(h1);                        % Analysis filter length
freq_samp = 200;                                     % Freq. samples in -pi to pi
freq_bound = pi;                                    % Interested range [pi]
freq_range_wrap = linspace(-pi, pi, freq_samp+1);   % Freq. of interest
freq_range = freq_range_wrap(2:end);
freq_shift_range = 0:3;                             % Sub-channel freq. shift (in BB)
M = length(freq_shift_range);                       % Number of sub-channels
P = M;                                              % Aliasing terms

response = zeros(freq_samp, M);

% random mismatch generation
NN = 12;
hunit = max(h1)/(2^NN - 1);
code = (h1./hunit);
code = round(code);
hq = hunit.*code;
r = 0 + hunit*randn(P*RFFIR_length,1)/3; %upper bound 3 sigma to be 10-bit
% all filters have different radom mismatch
for ii = 1:P
   hqr(:,ii) = r((ii - 1)*RFFIR_length + 1:ii*RFFIR_length) + h1;
end

%all filters have the same mismatch
% h1 = h1 + r(1:16);

% calculate response using mismatched analysis filters
for pp = 1:P
    for ff = 1:freq_samp
        freq = freq_range(ff);
        for mm = 1:P
            freq_shift = freq_shift_range(mm);
% here the "if mm == 1" and "if mm == 1 || mm == P" are mutually explusive
% to each other, used for the highest-frequency channel centered at <pi or
% at exactly pi
%             if mm == 1
%                 response(ff,mm,pp) = sum(hqr(:,pp).*cos(2*freq_shift*(pi/(2*M-1))*(0:RFFIR_length-1).').*exp(-1j*(freq - 2*pi*(pp - 1)/M)*(0:RFFIR_length-1).'));
%             else
%                 response(ff,mm,pp) = sum(2*hqr(:,pp).*cos(2*freq_shift*(pi/(2*M-1))*(0:RFFIR_length-1).').*exp(-1j*(freq - 2*pi*(pp - 1)/M)*(0:RFFIR_length-1).'));
%             end
            if mm == 1 || mm == P
                response(ff,mm,pp) = sum(hqr(:,mm).*cos(freq_shift*(pi/(M-1))*(0:RFFIR_length-1).').*exp(-1j*(freq - 2*pi*(pp - 1)/M)*(0:RFFIR_length-1).'));
            else
                response(ff,mm,pp) = sum(2*hqr(:,mm).*cos(freq_shift*(pi/(M-1))*(0:RFFIR_length-1).').*exp(-1j*(freq - 2*pi*(pp - 1)/M)*(0:RFFIR_length-1).'));
            end
        end
    end
end

% calculate response using identical analysis filters
for pp = 1:P
    for ff = 1:freq_samp
        freq = freq_range(ff);
        for mm = 1:P
            freq_shift = freq_shift_range(mm);
%             if mm == 1
%                 response(ff,mm,pp) = sum(h1.*cos(2*freq_shift*(pi/(2*M-1))*(0:RFFIR_length-1).').*exp(-1j*(freq - 2*pi*(pp - 1)/M)*(0:RFFIR_length-1).'));
%             else
%                 response(ff,mm,pp) = sum(2*h1.*cos(2*freq_shift*(pi/(2*M-1))*(0:RFFIR_length-1).').*exp(-1j*(freq - 2*pi*(pp - 1)/M)*(0:RFFIR_length-1).'));
%             end
            if mm == 1 || mm == P
                response_ideal(ff,mm,pp) = sum(h1.*cos(freq_shift*(pi/(M-1))*(0:RFFIR_length-1).').*exp(-1j*(freq - 2*pi*(pp - 1)/M)*(0:RFFIR_length-1).'));
            else
                response_ideal(ff,mm,pp) = sum(2*h1.*cos(freq_shift*(pi/(M-1))*(0:RFFIR_length-1).').*exp(-1j*(freq - 2*pi*(pp - 1)/M)*(0:RFFIR_length-1).'));
            end
        end
    end
end

% 2nd-order Butterworth (don't need to care)
% for pp = 1:P
%     for ff = 1:freq_samp
%         freq = freq_range(ff);
%         for mm = 1:P
%             freq_shift = freq_shift_range(mm);
%             if mm == 1 || mm == P
%                 response(ff,mm,pp) = sum(hIIR.*cos(freq_shift*(pi/(M-1))*(0:length(hIIR)-1).').*exp(-1j*(freq - 2*pi*(pp - 1)/M)*(0:length(hIIR)-1).'));
%             else
%                 response(ff,mm,pp) = sum(2*hIIR.*cos(freq_shift*(pi/(M-1))*(0:length(hIIR)-1).').*exp(-1j*(freq - 2*pi*(pp - 1)/M)*(0:length(hIIR)-1).'));
%             end
%         end
%     end
% end
%%
figure
plot(freq_range/pi,20*log10(abs(response(:,:,1))),'linewidth',2)

% plot(freq_range/pi,20*log10(abs(response(:,1,2))),'linewidth',2)
% hold on
% plot(freq_range/pi,20*log10(abs(response(:,1,4))),'linewidth',2)

xlabel('Freq [\pi]')
ylabel('Response [dB]')
grid on
xlim([-1,1]);
ylim([-100,5]);

% for tps = 60:10:110
tps = 40;
%% Parameters for optimziation 
d_target = 45;                                  % Target group delay in BB
N_taps = d_target*2 + 1;                                    % Target synthesize filter taps
% x_vec = [];                                     % Opt variable to determine
c_vec = zeros(N_taps*M + 1,1);   
c_vec(1) = 1;

% Target response of cascaded system
g_mtx = zeros(P, length(freq_range));
g_mtx(1,:) = exp(-1j * freq_range * d_target);

% Vector r and i in eq(7)
F_Re_mtx = zeros(P, freq_samp, M*N_taps);
F_Im_mtx = zeros(P, freq_samp, M*N_taps);
F_Re_mtx_ideal = zeros(P, freq_samp, M*N_taps);
F_Im_mtx_ideal = zeros(P, freq_samp, M*N_taps);


Freq_mtx = (cos(freq_range.'*(0:N_taps-1)) - 1j*sin(freq_range.'*(0:N_taps-1)));
% eps = 1e-2; % value to make mod functioin hits boundary

for ff = 1:freq_samp
    for pp = 1:P
        for mm=1:M
            
            % Indecies in vector r_p and i_p before eq(6)
            vec_idx = (mm-1)*N_taps+1 : mm*N_taps;
            
            % Find the shifted response of M channlized RF filter
%             freq_shift_p = round(mod((ff-(pp-1)*freq_samp/M)-eps, freq_samp) + eps);
            
            % follow description before eq(6)
            F_Re_mtx(pp,ff,vec_idx) = real((response(ff,mm,pp).')*Freq_mtx(ff,:).')/M;
            F_Im_mtx(pp,ff,vec_idx) = imag((response(ff,mm,pp).')*Freq_mtx(ff,:).')/M;
            
            F_Re_mtx_ideal(pp,ff,vec_idx) = real((response_ideal(ff,mm,pp).')*Freq_mtx(ff,:).')/M;
            F_Im_mtx_ideal(pp,ff,vec_idx) = imag((response_ideal(ff,mm,pp).')*Freq_mtx(ff,:).')/M;
        end
    end
end
%% Optimization via SOCP
cvx_begin
% cvx_precision best
    variable x_vec(N_taps*M + 1,1)
    minimize(c_vec.'*x_vec);
    for pp = 1:P
        for ff=1:freq_samp
            weight = (freq_range(ff)>-0.8*pi) & (freq_range(ff)<0.8*pi);
            if pp == 1
                c_vec.'*x_vec >= 1*norm([0,squeeze(F_Re_mtx_ideal(pp,ff,:)).';0,squeeze(F_Im_mtx_ideal(pp,ff,:)).']*x_vec - [real(g_mtx(pp,ff));imag(g_mtx(pp,ff))],2)*weight;
            else
                c_vec.'*x_vec >= norm([0,squeeze(F_Re_mtx_ideal(pp,ff,:)).';0,squeeze(F_Im_mtx_ideal(pp,ff,:)).']*x_vec - [real(g_mtx(pp,ff));imag(g_mtx(pp,ff))],2)*weight;
            end
        end
    end
cvx_end

%% quantization, I guess you can ignore this part for now
% % for i = 14:24
% % B = i;
% 
% B = 12;
% % for ind = 1:P
% %     maxf = max(abs(x_vec((ind - 1)*N_taps + 2:ind*N_taps + 1)));
% %     bfint = ceil(log2(maxf));
% %     if bfint < 0
% %         bfint = 0;
% %     end
% %     y((ind - 1)*N_taps + 1:ind*N_taps) = quantizenumeric(x_vec((ind - 1)*N_taps + 2:ind*N_taps + 1),1,B,B - 1 - bfint,'fix');
% % end
% 
% % quantization scale applies to all filters
% maxf = max(abs(x_vec(2:end)));
% bfint = ceil(log2(maxf));
% if bfint < 0
%     bfint = 0;
% end
% y = quantizenumeric(x_vec(2:end),1,B,B - 1 - bfint,'fix').';

%% Quantize synthesis filters
response_FIR = zeros(P,freq_samp);
for pp=1:P
    for ff=1:freq_samp
        %with quantization
%         response_FIR(pp,ff) = norm([squeeze(F_Re_mtx(pp,ff,:)).';squeeze(F_Im_mtx(pp,ff,:)).']*y.',2)^2;
% without quantization
        response_FIR(pp,ff) = norm([squeeze(F_Re_mtx(pp,ff,:)).';squeeze(F_Im_mtx(pp,ff,:)).']*x_vec(2:end),2)^2;
% without quantization and mismatch
%         response_FIR(pp,ff) = norm([squeeze(F_Re_mtx_ideal(pp,ff,:)).';squeeze(F_Im_mtx_ideal(pp,ff,:)).']*x_vec(2:end),2)^2;
    end
end

%I don't quite remember the following commented code, you can ignore them
% Aalias(i - 13) = 10*log10(max(max(abs(response_FIR(2:end,7:194)))));
% end

% plot(14:24,Aalias)
% hold on
% end

figure
subplot(211)
plot(freq_range/pi, 10*log10(response_FIR(1,:)),'linewidth',2);hold on
xlabel('Freq [pi]')
ylabel('Gain [dB]')
grid on

% subplot(312)
% plot(freq_range/pi, phase(response_FIR(1,:)),'linewidth',2);hold on
% xlabel('Freq [pi]')
% ylabel('Phase [rad]')

subplot(212)
plot(freq_range/pi, 10*log10(response_FIR(2,:)),'linewidth',2);hold on
plot(freq_range/pi, 10*log10(response_FIR(3,:)),'linewidth',2);hold on
plot(freq_range/pi, 10*log10(response_FIR(4,:)),'linewidth',2);hold on
% plot(freq_range/pi, 10*log10(response_FIR(5,:)),'linewidth',2);hold on
% plot(freq_range/pi, 10*log10(response_FIR(6,:)),'linewidth',2);hold on
% plot(freq_range/pi, 10*log10(response_FIR(7,:)),'linewidth',2);hold on
% plot(freq_range/pi, 10*log10(response_FIR(8,:)),'linewidth',2);hold on

grid on
legend('T_1','T_2','T_3','T_4','T_5','T_6','T_7')
xlabel('Freq [pi]')
ylabel('Gain [dB]')