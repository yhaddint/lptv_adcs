clear all;clc;

%% Load analysis filter taps
load('data/143M2.mat');
h143 = h;
load('data/250M.mat');
h250 = h;
load('data/500M.mat');
h500 = h;
load('../h1.mat');
h1 = h;
load('../hButterworth.mat');
hIIR = impz(b,a,100);
load('../hIIR_trunc.mat');
% hIIR = impz(b,a,200);

%% freq. response of analysis filters
% fvtool(h143,1);

%% freq. response of analysis filters
RFFIR_length = length(h1);                        % Analysis filter length
freq_samp = 200;                                     % Freq. samples in -pi to pi
freq_bound = pi;                                    % Interested range [pi]
freq_range = linspace(-pi, pi, freq_samp);          % Freq. of interest
freq_shift_range = 0:4;                             % Sub-channel freq. shift (in BB)
M = length(freq_shift_range);                       % Number of sub-channels
P = M;                                              % Aliasing terms

response = zeros(freq_samp, M);

% NN = 10;
% hunit = max(h1)/(2^NN - 1);
% code = (h1./hunit);
% code = round(code);
% hq = hunit.*code;
% r = 0 + hunit*randn(P*RFFIR_length,1)/3;
% for ii = 1:P
%    hqr(:,ii) = r((ii - 1)*RFFIR_length + 1:ii*RFFIR_length) + h1;
% end
% for pp = 1:P
%     for ff = 1:freq_samp
%         freq = freq_range(ff);
%         for mm = 1:P
%             freq_shift = freq_shift_range(mm);
% %             if mm == 1 || mm == P
% %                 response(ff,mm,pp) = sum(hqr(:,pp).*cos(freq_shift*(pi/(M-1))*(0:RFFIR_length-1).').*exp(-1j*(freq - 2*pi*(pp - 1)/M)*(0:RFFIR_length-1).'));
% %             else
% %                 response(ff,mm,pp) = sum(2*hqr(:,pp).*cos(freq_shift*(pi/(M-1))*(0:RFFIR_length-1).').*exp(-1j*(freq - 2*pi*(pp - 1)/M)*(0:RFFIR_length-1).'));
% %             end
%             if mm == 1
%                 response(ff,mm,pp) = sum(hqr(:,pp).*cos(2*freq_shift*(pi/(2*M-1))*(0:RFFIR_length-1).').*exp(-1j*(freq - 2*pi*(pp - 1)/M)*(0:RFFIR_length-1).'));
%             else
%                 response(ff,mm,pp) = sum(2*hqr(:,pp).*cos(2*freq_shift*(pi/(2*M-1))*(0:RFFIR_length-1).').*exp(-1j*(freq - 2*pi*(pp - 1)/M)*(0:RFFIR_length-1).'));
%             end
%         end
%     end
% end

for pp = 1:P
    for ff = 1:freq_samp
        freq = freq_range(ff);
        for mm = 1:P
            freq_shift = freq_shift_range(mm);
%             if mm == 1 || mm == P
%                 response(ff,mm,pp) = sum(h1.*cos(freq_shift*(pi/(M-1))*(0:RFFIR_length-1).').*exp(-1j*(freq - 2*pi*(pp - 1)/M)*(0:RFFIR_length-1).'));
%             else
%                 response(ff,mm,pp) = sum(2*h1.*cos(freq_shift*(pi/(M-1))*(0:RFFIR_length-1).').*exp(-1j*(freq - 2*pi*(pp - 1)/M)*(0:RFFIR_length-1).'));
%             end
            if mm == 1
                response(ff,mm,pp) = sum(h1.*cos(2*freq_shift*(pi/(2*M-1))*(0:RFFIR_length-1).').*exp(-1j*(freq - 2*pi*(pp - 1)/M)*(0:RFFIR_length-1).'));
            else
                response(ff,mm,pp) = sum(2*h1.*cos(2*freq_shift*(pi/(2*M-1))*(0:RFFIR_length-1).').*exp(-1j*(freq - 2*pi*(pp - 1)/M)*(0:RFFIR_length-1).'));
            end
%             response(ff,mm,pp) = sum(2*h1.*cos((2*freq_shift + 1)*(pi/(2*M))*(0:RFFIR_length-1).').*exp(-1j*(freq - 2*pi*(pp - 1)/M)*(0:RFFIR_length-1).'));
        end
    end
end

% 2nd-order Butterworth
% for pp = 1:P
%     for ff = 1:freq_samp
%         freq = freq_range(ff);
%         for mm = 1:P
%             freq_shift = freq_shift_range(mm);
%             if mm == 1 || mm == P
%                 response(ff,mm,pp) = sum(hIIR.*cos(freq_shift*(pi/(M-1))*(0:length(hIIR)-1).').*exp(-1j*(freq - 2*pi*(pp - 1)/M)*(0:length(hIIR)-1).'));
%             else
%                 response(ff,mm,pp) = sum(2*hIIR.*cos(freq_shift*(pi/(M-1))*(0:length(hIIR)-1).').*exp(-1j*(freq - 2*pi*(pp - 1)/M)*(0:length(hIIR)-1).'));
%             end
%         end
%     end
% end

figure
plot(freq_range/pi,20*log10(abs(response(:,:,1))),'linewidth',2)
xlabel('Freq [\pi]')
ylabel('Response [dB]') 
grid on
xlim([-1,1]);
ylim([-100,5]);

for delay = 1:40
tps = 20;
N_taps = tps*2 + 1;                                    % Target synthesize filter taps
d_target = delay;                                  % Target group delay in BB
% x_vec = [];                                     % Opt variable to determine

% Target response of cascaded system
g_mtx = zeros(P, length(freq_range));
F = zeros(length(freq_range), P);
g_mtx(1,:) = exp(-1j * freq_range * d_target);


for ff = 1:freq_samp
    F(ff,:) = (g_mtx(:,ff).')*squeeze(response(ff,:,:)/5)^-1;
end

% figure
% plot(freq_range,20*log10(abs(F)),'linewidth',2)
% xlabel('Frequency ')
% ylabel('Response [dB]')
% grid on
% xlim([0,pi]);
% ylim([-50,30]);

for i = 1:P
    fFIR(:,i) = invfreqz(F(:,i),freq_range,N_taps-1,0);
end
f = reshape(fFIR,[],1).';

% Vector r and i in eq(7)
F_Re_mtx = zeros(P, freq_samp, M*N_taps);
F_Im_mtx = zeros(P, freq_samp, M*N_taps);


Freq_mtx = (cos(freq_range.'*(0:N_taps-1)) - 1j*sin(freq_range.'*(0:N_taps-1)));
% eps = 1e-2; % value to make mod functioin hits boundary

for ff = 1:freq_samp
    for pp = 1:P
        for mm=1:M
            
            % Indecies in vector r_p and i_p before eq(6)
            vec_idx = (mm-1)*N_taps+1 : mm*N_taps;
            
            % Find the shifted response of M channlized RF filter
%             freq_shift_p = round(mod((ff-(pp-1)*freq_samp/M)-eps, freq_samp) + eps);
            
            % follow description before eq(6)
            F_Re_mtx(pp,ff,vec_idx) = real((response(ff,mm,pp).')*Freq_mtx(ff,:).')/M;
            F_Im_mtx(pp,ff,vec_idx) = imag((response(ff,mm,pp).')*Freq_mtx(ff,:).')/M;
        end
    end
end

B = 16;
% for ind = 1:P
%     maxf = max(abs(x_vec((ind - 1)*N_taps + 2:ind*N_taps + 1)));
%     bfint = ceil(log2(maxf));
%     if bfint < 0
%         bfint = 0;
%     end
%     y((ind - 1)*N_taps + 1:ind*N_taps) = quantizenumeric(x_vec((ind - 1)*N_taps + 2:ind*N_taps + 1),1,B,B - 1 - bfint,'fix');
% end

% quantization scale applies to all filters
maxf = max(abs(f));
bfint = ceil(log2(maxf));
if bfint < 0
    bfint = 0;
end
y = quantizenumeric(f,1,B,B - 1 - bfint,'fix').';

response_FIR = zeros(P,freq_samp);
for pp=1:P
    for ff=1:freq_samp
%         response_FIR(pp,ff) = norm([squeeze(F_Re_mtx(pp,ff,:)).';squeeze(F_Im_mtx(pp,ff,:)).']*y,2)^2;
        response_FIR(pp,ff) = norm([squeeze(F_Re_mtx(pp,ff,:)).';squeeze(F_Im_mtx(pp,ff,:)).']*f.',2)^2;
    end
end

% Aalias(i - 13) = 10*log10(max(max(abs(response_FIR(2:end,7:194)))));
% end

% plot(14:24,Aalias)
% hold on
% end

% figure
% subplot(211)
% plot(freq_range/pi, 10*log10(response_FIR(1,:)),'linewidth',2);hold on
% xlabel('Freq [pi]')
% ylabel('Gain [dB]')
% grid on
% 
% % subplot(312)
% % plot(freq_range/pi, phase(response_FIR(1,:)),'linewidth',2);hold on
% % xlabel('Freq [pi]')
% % ylabel('Phase [rad]')
% 
% subplot(212)
% plot(freq_range/pi, 10*log10(response_FIR(2,:)),'linewidth',2);hold on
% plot(freq_range/pi, 10*log10(response_FIR(3,:)),'linewidth',2);hold on
% plot(freq_range/pi, 10*log10(response_FIR(4,:)),'linewidth',2);hold on
% % plot(freq_range/pi, 10*log10(response_FIR(5,:)),'linewidth',2);hold on
% % plot(freq_range/pi, 10*log10(response_FIR(6,:)),'linewidth',2);hold on
% % plot(freq_range/pi, 10*log10(response_FIR(7,:)),'linewidth',2);hold on
% % plot(freq_range/pi, 10*log10(response_FIR(8,:)),'linewidth',2);hold on
% 
% grid on
% legend('T_1','T_2','T_3','T_4','T_5','T_6','T_7')
% xlabel('Freq [pi]')
% ylabel('Gain [dB]')

Aalias(delay) = max(max(response_FIR(2:end,:)));
end
figure
plot(10*log10(Aalias))